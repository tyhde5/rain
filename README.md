﻿# Rainfall Model!




## Pour commencer

Tout d'abord, il faut installer les différents packages:

    install.packages("ggplot2")
    install.packages("gifski")
    install.packages("lubridate")
    install.packages("tidyverse")
    install.packages("gtools")
    install.packages("png")
    install.packages("qapi")
    install.packages("pracma")
    install.packages("msos")
Définir l'environnement de travail:

    setwd("le chemin vers le dossier rain")

Editer la liste des fichiers:

    fil <- c("data/Event1_1.csv","data/Event1_2.csv")
## Format des données

Les données sont au format CSV. Avec pour séparateur ";".
Il faut leur donner une entête, de la forme suivante:

    A;D;M;H; Mi;t;RainRate
   Où `A` est l'année, `D` le jour, `M` le mois, `H` l'heure, `Mi` les minutes, `t` les secondes, `RainRate` l'intensité de la pluie.

> A;D;M;H; Mi;t;RainRate
2.0170000e+03;8.0000000e+00;8.0000000e+00;4.0000000e+00;1.0000000e+00;0.0000000e+00;0.0000000e+00
2.0170000e+03;8.0000000e+00;8.0000000e+00;4.0000000e+00;3.0000000e+00;0.0000000e+00;0.0000000e+00

Il faut modifier le fichier **XY.csv** afin d'ajouter les coordonnées des différents capteurs, sans entête, avec comme séparateur ";".

> 1;573436;117843
2;573272;119709
3;573436;117843

**MeasuredRainTS** est une liste de dataframe:

      # Liste  des temps en seconde commençant à 0 du capteur i
      MeasuredRainTS[[i]]$t
      # Liste des niveaux de pluie du capteur i
      MeasuredRainTS[[i]]$RainRate
   
   Coordonnées des capteurs:
   
      Co[i,2] # Position relative X du capteur i
      Co[i,3] # Position relative Y du capteur i



