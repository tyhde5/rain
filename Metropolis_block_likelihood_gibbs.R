Metropolis_block_likelihood_gibbs <-
  function(m0,
           V_step_walk,
           M_prior,
           MeasuredRainTS,
           size_block,
           nb_ep_warmup,
           nb_sample,
           step,
           nb_iter_gibbs,
           Co) {
    cat("Prepare data\n")
    
    
    nb_ep_mes <- length(MeasuredRainTS[[1]]$RainRate)
    nb_blocks <- floor(nb_ep_mes / size_block)
    
    size_block_coord <- length(MeasuredRainTS) * size_block
    
    #ind_Z pas utilisé
    
    Coord <- list()
    Rm <- list()
    V_tot <- list()
    
    for (b in 1:nb_blocks) {
      for (i in 1:size_block) {
        for (j in 1:length(MeasuredRainTS)) {
          # Co[j,2] = X du capteur j
          # Co[j,3] = Y du capteur j
          
          Coord <-
            rbind(Coord, c(Co[j, 2], Co[j, 3], MeasuredRainTS[[j]]$t[[(b - 1) * size_block +
                                                                        i]]))
          Rm[length(Rm) + 1] <-
            list(c(MeasuredRainTS[[j]]$RainRate[[(b - 1) * size_block + i]]))
        }
      }
    }
    Z_k <- Rm
    
    for (i in 1:length(Z_k)) {
      if (Rm[[i]] == 0) {
        Z_k[i] = abs(-2 - m0[[7]]) * runif(1) - 2
      }
    }
    
    cat("Initialization\n")
    
    
    resApplyModel <- apply_model(m0, Coord, size_block_coord, Rm, Z_k)
    Sigma_k <-  resApplyModel$Sigma
    inv_Sigma_k <- resApplyModel$inv_Sigma
    Z_k <- resApplyModel$Z
    
    Z_k <-
      gibbs_update_Z0(m0, Z_k, Rm, inv_Sigma_k, size_block_coord, 500)
    
    
    cat("Sampling\n")
    
    #V_tot <- matrix(0, nb_ep_warmup+nb_sample, length(m0)+2+1)
    V_sample <-
      matrix(0, nrow = nb_sample, ncol = (length(m0) + length(Z_k)))
    nb_accept <- 0
    alpha_covariance <- 0
    alpha_anamorphosis <- 0
    
    q0 <- matrix(1, length(m0), 1)
    
    for (i in 1:(nb_ep_warmup + nb_sample * step)) {
      L0 <-
        likelihood_tot(Sigma_k, inv_Sigma_k, Z_k, Rm, m0, size_block_coord)
      
      resRandomWalk <- random_walk(m0, q0, V_step_walk, M_prior)
      m1 <- resRandomWalk$m1
      q1 <- resRandomWalk$q1
      
      
      
      resApplyModel <- apply_model(m1, Coord, size_block_coord, Rm, Z_k)
      Sigma_p <-  resApplyModel$Sigma
      inv_Sigma_p <- resApplyModel$inv_Sigma
      
      Z_p <- resApplyModel$Z
      
      L1 <-
        likelihood_tot(Sigma_p, inv_Sigma_p, Z_p, Rm, m1, size_block_coord)
      alpha <- exp(L1 - L0) * prod(q1) / prod(q0)
      # a voir prod
      if (alpha > 1 || runif(1) > (1 - alpha)) {
        m0 <- m1
        
        q0 <- q1
        Sigma_k <- Sigma_p
        inv_Sigma_k <- inv_Sigma_p
        Z_k <- Z_p
        
        Z_k <-
          gibbs_update_Z0(m0, Z_k, Rm , inv_Sigma_k, size_block_coord , nb_iter_gibbs)
        
        nb_accept <- nb_accept + 1
        
        
      }
      
      
      V_tot <-
        rbind(V_tot, c(m0, alpha_anamorphosis, alpha_covariance, L1))
      cat(i)
      cat("\n")
      
      if (mod((i - nb_ep_warmup), step) == 0 &&
          (i > nb_ep_warmup)) {
        m <- c(m0, Z_k)
        for (g in 1:length(m)) {
          V_sample[floor((i - nb_ep_warmup) / step), g] = m[[g]]
        }
      }
    }
    # en sortie
    return(list(V_tot = V_tot, V_sample = V_sample))
  }

#Likelihood functions
likelihood_tot <-
  function(Sigma,
           inv_Sigma,
           Z,
           Rm,
           m,
           size_block_coord) {
    nb <- length(Z) / size_block_coord
    sum_log_likelihood_structure <- 0
    sum_log_likelihood_anamorphose <- 0
    
    for (i1 in 1:nb) {
      Rmb = Rm[((i1 - 1) * size_block_coord + 1):(i1 * size_block_coord)]
      Zb = c(((i1 - 1) * size_block_coord + 1):(i1 * size_block_coord))
      
      NI <- sum(Rmb > 0)
      
      log_likelihood_structure <-
        -0.5 * logdet(Sigma) - 0.5 * Zb * inv_Sigma * Zb - 0.5 * NI * log(2 * pi)
      sum_log_likelihood_structure <-
        sum_log_likelihood_structure + log_likelihood_structure
      
      log_likelihood_anamorphose <- 0
      nb_pos_data <- 0
      
      for (i2 in 1:size_block_coord) {
        if (Rmb[[i2]] > 0.5) {
          L <- m[[8]] * m[[9]] * Rmb[[i2]] ^ (m[[9]] - 1)
          log_likelihood_anamorphose <-
            log_likelihood_anamorphose + log(abs(L))
          nb_pos_data <- nb_pos_data + 1
        }
        
      }
      sum_log_likelihood_anamorphose = sum_log_likelihood_anamorphose + log_likelihood_anamorphose
    }
    sum_log_likelihood = sum_log_likelihood_structure + sum_log_likelihood_anamorphose
    
    if (!is.finite(sum_log_likelihood) || is.nan(sum_log_likelihood)) {
      cat("Undefined likelihood")
    }
    
    #en sortie
    return(sum_log_likelihood)
  }



#Gibbs sampler
gibbs_update_Z0 <-
  function(m,
           Z,
           Rm,
           inv_Sigma,
           size_block_coord,
           nb_iter_gibbs) {
    for (i_gibbs in 1:nb_iter_gibbs) {
      Z0 <- Z[1:size_block_coord]
      Rmb <- Rm[1:size_block_coord]
      
      for (i2 in 1:(size_block_coord - 1)) {
        it = 1
        
        if (Rm[[i2]] == 0) {
          resCondNorSim <- conditional_normal_sim(inv_Sigma, i2, Z0)
          sigma_gibbs <- resCondNorSim$sigma
          mu_gibbs <- resCondNorSim$mu
          sim_value <- mu_gibbs + sigma_gibbs * rnorm(1)
          while (sim_value > m[[7]] && it < 40) {
            sim_value <- mu_gibbs + rnorm(1) * sigma_gibbs
            it <- it + 1
            if (it > 39) {
              sim_value = Z0[[i2]]
              
            }
          }
          Z0[[i2]] <- sim_value
        }
      }
      Z[1:size_block_coord] <- Z0
      
      
      Zf <- tail(Z, size_block_coord)
      Rmb <- tail(Rm, size_block_coord)
      for (i2 in 1:(size_block_coord - 1)) {
        it <- 1
        
        if (Rmb[[i2]] == 0) {
          resCondNorSim <- conditional_normal_sim(inv_Sigma, i2, Zf)
          sigma_gibbs <- resCondNorSim$sigma
          mu_gibbs <- resCondNorSim$mu
          
          sim_value <- mu_gibbs + rnorm(1) * sigma_gibbs
          while ((sim_value > m[[7]]) && (it < 40)) {
            sim_value <- mu_gibbs + rnorm(1) * sigma_gibbs
            it <- it + 1
            if (it > 39) {
              sim_value = Zf[[i2]]
            }
          }
          Zf[[i2]] <- sim_value
          
        }
        
      }
      Z[(length(Z) - size_block_coord + 1):length(Z)] <- Zf
    }
    
    for (ic in (size_block_coord + 1):(length(Z) - size_block_coord - 1)) {
      it <- 1
      
      if (Rm[[ic]] == 0) {
        ind_ini_Zc = ic - floor(size_block_coord / 2)
        Zc <- Z[ind_ini_Zc:(ind_ini_Zc + size_block_coord - 1)]
        
        resCondNorSim <-
          conditional_normal_sim(inv_Sigma, floor(size_block_coord / 2) + 1, Zc)
        sigma_gibbs <- resCondNorSim$sigma
        mu_gibbs <- resCondNorSim$mu
        
        sim_value <- mu_gibbs + rnorm(1) * sigma_gibbs
        # A VOIR pour le -3
        while ((sim_value > m[[7]] || sim_value < (-3)) && it < 40) {
          sim_value = mu_gibbs + rnorm(1) * sigma_gibbs
          it <- it + 1
          if (it > 39) {
            sim_value <- Z[[ic]]
            
          }
        }
        Z[[ic]] <- sim_value
      }
      
    }
    
    #en sortie
    cat("Fin blocs\n")
    
    return(Z)
  }


apply_model <- function(m, Coord, size_block_coord, Rm, Z) {
  #Marginal distribution
  
  
  for (jj in 1:length(Rm)) {
    if (Rm[[jj]] > 0) {
      Z[[jj]] = m[[8]] * (Rm[[jj]] ^ m[[9]]) + m[[7]]
    }
  }
  
  
  #Covariance structure
  my_coord <- Coord[1:size_block_coord, ]
  
  if (m[[10]] == 0 && m[[11]] == 0) {
    Coord_Lagrang <- my_coord[, 1]
    Coord_Lagrang <- cbind(Coord_Lagrang, my_coord[, 2])
    Coord_Lagrang <- cbind(Coord_Lagrang, my_coord[, 3])
  } else{
    Coord_Lagrang <-
      mapply("-", my_coord[, 1], mapply("*", my_coord[, 3], m[[10]] * cos(m[[11]] *
                                                                            pi / 180)))
    Coord_Lagrang <-
      cbind(Coord_Lagrang, mapply("-", my_coord[, 2], mapply("*", my_coord[, 3], m[[10]] *
                                                               sin(m[[11]] * pi / 180))))
    Coord_Lagrang <- cbind(Coord_Lagrang,  my_coord[, 3])
  }
  
  V_X <-
    matrix(Coord_Lagrang[, 1],
           nrow = length(Coord_Lagrang[, 1]),
           ncol = 1)
  V_Y <-
    matrix(Coord_Lagrang[, 2],
           nrow = length(Coord_Lagrang[, 2]),
           ncol = 1)
  V_Z <-
    matrix(Coord_Lagrang[, 3],
           nrow = length(Coord_Lagrang[, 3]),
           ncol = 1)
  
  
  MVX1 <- repMatColVRAI(V_X, length(V_X))
  MVX2 <- repMatLigneVRAI(V_X, length(V_X))
  
  MVY1 <- repMatColVRAI(V_Y, length(V_Y))
  MVY2 <- repMatLigneVRAI(V_Y, length(V_Y))
  
  MVY1 <- matrix(MVY1, length(V_Y), length(V_Y))
  MVY2 <- matrix(MVY2, length(V_Y), length(V_Y))
  
  MVX1 <- matrix(MVX1, length(V_X), length(V_X))
  MVX2 <- matrix(MVX2, length(V_X), length(V_X))
  
  
  M_ds = sqrt((MVX2 - MVX1) ^ 2 + (MVY2 - MVY1) ^ 2)
  
  MVt1 <- repMatColVRAI(V_Z, length(V_Z))
  MVt2 <- repMatLigneVRAI(V_Z, length(V_Z))
  MVt1 <- matrix(MVt1, length(V_Z), length(V_Z))
  MVt2 <- matrix(MVt2, length(V_Z), length(V_Z))
  rm(V_Z)
  rm(V_X)
  rm(V_Y)
  rm(MVX1)
  rm(MVX2)
  rm(MVY1)
  rm(MVY2)
  
  M_dt <- abs(MVt2 - MVt1)
  rm(MVt2)
  rm(MVt1)
  
  to <- 1
  c = m[[1]] ^ (-2 * m[[2]])
  a = m[[3]] ^ (-2 * m[[4]])
  
  Elem <- a * M_dt ^ (2 * m[[4]]) + 1
  rm(M_dt)
  Sigma <-
    1. / (Elem ^ to) * exp(-c * (M_ds ^ (2. * m[[2]])) / (Elem ^ (m[[5]] *
                                                                    m[[2]])))
  rm(Elem)
  rm(M_ds)
  
  Sigma <- (1 - m[[6]] ^ 2) * Sigma + diag(size_block_coord) * (m[[6]] ^
                                                                  2)
  
  inv_Sigma <- solve(Sigma)
  
  
  # en sortie
  return(list(
    Sigma = Sigma,
    inv_Sigma = inv_Sigma,
    Z = Z
  ))
}


random_walk <- function(m0, q0, V_step_walk, M_prior) {
  m1 <- m0
  q1 <- q0
  
  #spatial range
  step_walk <- V_step_walk[1]
  B1 <- max(M_prior[1, 1], m0[1] - step_walk)
  B2 <- min(M_prior[1, 2], m0[1] + step_walk)
  m1[1] <- runif(1) * (B2 - B1) + B1
  q1[1] <- 1 / (B2 - B1)
  
  #space regularity parameter (smoothness)
  step_walk <- V_step_walk[2]
  B1 <- max(M_prior[2, 1], m0[[2]] - step_walk)
  B2 <- min(M_prior[2, 2], m0[[2]] + step_walk)
  m1[2] <- runif(1) * (B2 - B1) + B1
  q1[2] <- 1 / (B2 - B1)
  
  #temporal range
  step_walk <- V_step_walk[3]
  B1 <- max(M_prior[3, 1], m0[3] - step_walk)
  B2 <- min(M_prior[3, 2], m0[3] + step_walk)
  m1[3] <- runif(1) * (B2 - B1) + B1
  q1[3] <- 1 / (B2 - B1)
  
  #time regularity parameter (smoothness)
  step_walk = V_step_walk[4]
  B1 = max(M_prior[4, 1], m0[4] - step_walk)
  B2 = min(M_prior[4, 2], m0[4] + step_walk)
  m1[4] = runif(1) * (B2 - B1) + B1
  q1[4] = 1 / (B2 - B1)
  
  #space-time interaction parameter
  step_walk <- V_step_walk[5]
  
  B1 <- max(M_prior[5, 1], m0[5] - step_walk)
  B2 <- min(M_prior[5, 2], m0[5] + step_walk)
  m1[5] <- runif(1) * (B2 - B1) + B1
  q1[5] <- 1 / (B2 - B1)
  
  #noise
  step_walk <- V_step_walk[6]
  B1 <- max(M_prior[6, 1], m0[6] - step_walk)
  B2 <- min(M_prior[6, 2], m0[6] + step_walk)
  m1[6] <- runif(1) * (B2 - B1) + B1
  q1[6] <- 1 / (B2 - B1)
  
  #advection velocity
  step_walk <- V_step_walk[10]
  B1 <- max(M_prior[10, 1], m0[10] - step_walk)
  B2 <- min(M_prior[10, 2], m0[10] + step_walk)
  m1[10] <- runif(1) * (B2 - B1) + B1
  q1[10] <- 1 / (B2 - B1)
  
  #advection direction
  step_walk = V_step_walk[11]
  B1 <- max(M_prior[11, 1], m0[11] - step_walk)
  B2 <- min(M_prior[11, 2], m0[11] + step_walk)
  m1[11] <- runif(1) * (B2 - B1) + B1
  q1[11] <- 1 / (B2 - B1)
  
  #a1
  step_walk <- V_step_walk[8]
  B1 <- max(M_prior[8, 1], m0[8] - step_walk)
  B2 <- min(M_prior[8, 2], m0[8] + step_walk)
  m1[8] <- runif(1) * (B2 - B1) + B1
  q1[8] <- 1 / (B2 - B1)
  
  #a2
  step_walk <- V_step_walk[9]
  B1 <- max(M_prior[9, 1], m0[9] - step_walk)
  B2 <- min(M_prior[9, 2], m0[9] + step_walk)
  m1[9] <- runif(1) * (B2 - B1) + B1
  q1[9] <- 1 / (B2 - B1)
  
  # en sortie
  return(list(m1 = m1, q1 = q1))
  
}

repMatColVRAI <- function(M, n) {
  A <- matrix(0, n, n)
  for (i in 1:n) {
    for (j in 1:n) {
      A[i, j] <- M[[i]]
    }
  }
  return(A)
}


repMatLigneVRAI <- function(M, n) {
  A <- matrix(0, n, n)
  for (i in 1:n) {
    for (j in 1:n) {
      A[i, j] <- M[[j]]
    }
  }
  return(A)
}
