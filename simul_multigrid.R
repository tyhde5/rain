simul_multigrid <-
  function(Coord_Euler,
           CondiRainTS_gaussian,
           m,
           nb_ep,
           step_t,
           nb_max_cond) {
    M_res_sim <- matrix(0, length(Coord_Euler), nb_ep)
    Path_to_sim <- c(1, nb_ep)
    To_sim <- 2:(nb_ep - 1)
    iter <- 1
    
    ind_min <- 0
    ind_max <- 1
    
    while (!(length(To_sim) == 0)) {
      V_path_temp <- list()
      sim_path_sort <- sort(unlist(Path_to_sim)) #unlist ?
      for (i in 2:length(sim_path_sort)) {
        ind_min <- sim_path_sort[i - 1] + 1
        ind_max <- sim_path_sort[i] - 1
        if (ind_min > ind_max) {
          Path_to_sim <- c(Path_to_sim, To_sim)
          To_sim <- list()
          break
        }
        bmin <- match(ind_min, To_sim)
        bmax <- match(ind_max, To_sim)
        temp <- floor(median(To_sim[bmin:bmax]))
        rem <- match (temp, To_sim)
        To_sim <- c(To_sim[1:(rem - 1)], To_sim[(rem + 1):length(To_sim)])
        V_path_temp[length(V_path_temp) + 1] <- temp
      }
      if (!(length(V_path_temp) == 0)) {
        Path_to_sim <- c(Path_to_sim, V_path_temp)
      }
      iter <- iter + 1
    }
    
    Simulated_pts <- list()
    for (i in 1:length(Path_to_sim)) {
      cat("Random path ")
      cat(i)
      cat("\n")
      
      if (!length(Simulated_pts) == 0) {
        for (g in 1:length(Simulated_pts)) {
          V_dist_t <- abs(Simulated_pts[[g]] - Path_to_sim[[i]])
        }
      } else{
        #départ TODO
        V_dist_t <- list()
      }
      
      
      V_dist_tempo_sort <- sort(unlist(V_dist_t))
      
      ind_sort <- min(nb_max_cond, length(V_dist_t))
      
      if (ind_sort > 0) {
        dist_max <- V_dist_tempo_sort[ind_sort]
        V_ind_cond <- findInListInf(V_dist_t, dist_max)
        
        pts_condi <- list()
        Z_condi <- list()
        
        for (j in 1:length(V_ind_cond)) {
          for (k in 1:length(Coord_Euler[, 1])) {
            tempMa <- matrix(0, length(Simulated_pts[V_ind_cond[[j]]]), 3)
            for (g in 1:length(Simulated_pts[V_ind_cond[[j]]])) {
              tempMa[g, 1] <-
                Coord_Euler[[k, 1]] - Simulated_pts[V_ind_cond[[j]]][[g]] * step_t * m[10] *
                cos(m[11] * pi / 180)
              tempMa[g, 2] <-
                Coord_Euler[[k, 2]] -  Simulated_pts[V_ind_cond[[j]]][[g]] * step_t * m[10] *
                sin(m[11] * pi / 180)
              tempMa[g, 3] <-
                Simulated_pts[V_ind_cond[[j]]][[g]] * step_t
              
            }
            
            
            pts_condi <- rbind(pts_condi, tempMa)
            Z_condi <-
              rbind(Z_condi, M_res_sim[k, Simulated_pts[[V_ind_cond[[j]]]]])
          }
        }
        tmin <- minSpe(Simulated_pts, V_ind_cond) * step_t
        tmax <- maxSpe(Simulated_pts, V_ind_cond) * step_t
        
        
        nb_harddata <- 0
        
        for (j in 1:length(CondiRainTS_gaussian)) {
          my_idx <- findInList2(CondiRainTS_gaussian, Path_to_sim, i, j, step_t)
          ind_min = min(unlist(my_idx))
          ind_max = max(unlist(my_idx))
          
          
          tempMa <- matrix(0, ind_max - ind_min + 1, 3)
          
          for (k in ind_min:ind_max) {
            tempMa[k - ind_min, 1] <-
              CondiRainTS_gaussian[[j]]$X[[k]] - CondiRainTS_gaussian[[j]]$t[[k]] * m[10] *
              cos(m[11] * pi / 180)
            tempMa[k - ind_min, 2] <-
              CondiRainTS_gaussian[[j]]$Y[[k]] - CondiRainTS_gaussian[[j]]$t[[k]] * m[10] *
              sin(m[11] * pi / 180)
            tempMa[k - ind_min, 3] <- CondiRainTS_gaussian[[j]]$t[[k]]
            
          }
          
          pts_condi <- rbind(pts_condi, tempMa)
          
          for (k in ind_min:ind_max) {
            Z_condi <- rbind(Z_condi, CondiRainTS_gaussian[[j]]$RainRate[[k]])
          }
          
          
          
          nb_harddata <-
            nb_harddata + length(CondiRainTS_gaussian[[j]]$RainRate[ind_min:ind_max])
        }
        
      } else{
        pts_condi <- list()
        Z_condi <- list()
        nb_harddata <- 0
        
        for (j in 1:length(CondiRainTS_gaussian)) {
          my_idx <- findInList2(CondiRainTS_gaussian, Path_to_sim, i, j, step_t)
          
          ind_min = min(unlist(my_idx))
          ind_max = max(unlist(my_idx))
          
          
          tempMa <- matrix(0, ind_max - ind_min + 1, 3)
          
          for (k in ind_min:ind_max) {
            tempMa[k - ind_min + 1, 1] <-
              CondiRainTS_gaussian[[j]]$X[[k]] - CondiRainTS_gaussian[[j]]$t[[k]] * m[10] *
              cos(m[11] * pi / 180)
            tempMa[k - ind_min + 1, 2] <-
              CondiRainTS_gaussian[[j]]$Y[[k]] - CondiRainTS_gaussian[[j]]$t[[k]] * m[10] *
              sin(m[11] * pi / 180)
            tempMa[k - ind_min + 1, 3] <-
              CondiRainTS_gaussian[[j]]$t[[k]]
            
          }
          
          pts_condi <- rbind(pts_condi, tempMa)
          
          for (k in ind_min:ind_max) {
            Z_condi <- rbind(Z_condi, CondiRainTS_gaussian[[j]]$RainRate[[k]])
          }
          nb_harddata <- nb_harddata + ind_max - ind_min
          
        }
      }
      pts_target <- list()
      for (k in 1:length(Coord_Euler[, 1])) {
        temp <-
          c(
            Coord_Euler[k, 1][[1]] - Path_to_sim[[i]] * step_t * m[10] * cos(m[11] *
                                                                               pi / 180),
            Coord_Euler[k, 2][[1]] - Path_to_sim[[i]] * step_t * m[10] * sin(m[11] *
                                                                               pi / 180),
            Path_to_sim[[i]] * step_t
          )
        pts_target <- rbind(pts_target, temp)
        
      }
      
      if (!length(pts_condi) == 0) {
        Sigma_target <- M_cov_target(pts_target, m)
        Sigma_condi <- M_cov_obs(pts_condi, nb_harddata, m)
        inv_Sigma_condi <- solve(Sigma_condi)
        Sigma_cross_cov <-
          M_cross_cov(pts_target, pts_condi, nb_harddata, m)
      } else{
        Z_condi <- 0
        Sigma_cross_cov <- 0
        inv_Sigma_condi <- 0
        Sigma_target <- M_cov_target(pts_target, m)
      }
      Z_target <-
        core_simul(Z_condi, Sigma_cross_cov, Sigma_target, inv_Sigma_condi)
      for (k in 1:length(Coord_Euler[, 1])) {
        M_res_sim[k, Path_to_sim[[i]]] <- Z_target[[k]]
      }
      
      
    }
    SimulatedRainTS <- list()
    for (i in 1:length(Coord_Euler[, 1])) {
      XTEMP <-  Coord_Euler[i, 1]
      YTEMP <- Coord_Euler[i, 2]
      tTEMP <- matrix(0, length(Path_to_sim), 1)
      RainRateTEMP <- matrix(0, length(Path_to_sim), 1)
      for (j in 1:nb_ep) {
        tTEMP[j] <- j * step_t
        RainRateTEMP[j] <- M_res_sim[i, j]
      }
      df <- data.frame (
        X  = unlist(XTEMP),
        Y = unlist(YTEMP),
        t = unlist(tTEMP),
        RainRate = unlist(RainRateTEMP)
      )
      SimulatedRainTS[[i]] <- df
      
    }
    
    return(SimulatedRainTS)
  }


core_simul <-
  function(Z_obs,
           Sigma_cross_cov,
           Sigma_target,
           inv_Sigma_obs) {
    Sigma_cross_cov <-
      matrix(
        Sigma_cross_cov,
        nrow = nrow(Sigma_cross_cov),
        ncol = ncol(Sigma_cross_cov)
      )
    inv_Sigma_obs <-
      matrix(inv_Sigma_obs,
             nrow = nrow(inv_Sigma_obs),
             ncol = ncol(inv_Sigma_obs))
    
    
    Z_obs <- matrix(Z_obs, nrow = nrow(Z_obs), ncol = 1)
    V_IID <- rnorm(length(Sigma_target[, 1])) #TODO Check Others Runif
    V_IID <- matrix(V_IID, ncol = 1 , nrow = length(Sigma_target[, 1]))
    
    muTEMP <- t(Sigma_cross_cov %*% inv_Sigma_obs)
    
    
    mu <- matrix(0, length(Sigma_target[, 1]), 1)
    for (o in 1:length(Sigma_target[, 1])) {
      evaluationMu <- 0
      for (g in 1:nrow(muTEMP)) {
        evaluationMu <- evaluationMu + muTEMP[g, o] * Z_obs[[g]]
      }
      mu[o] <- evaluationMu
    }
    
    rm(Z_obs)
    
    Sigma <-
      Sigma_target - Sigma_cross_cov %*% inv_Sigma_obs %*% t(Sigma_cross_cov)
    rm(inv_Sigma_obs)
    L <- chol(Sigma)
    
    Z_target <- mu + L %*% V_IID
    rm(V_IID)
    return(Z_target)
    
    }
repmatLigne = function(X, n) {
  matrix(rep(t(X) , n) , ncol = ncol(X) , byrow = TRUE)
}
repmatColonne = function(X, n) {
  matrix(rep(t(X) , n) , nrow = nrow(X) , byrow = F)
}

M_cov_target <- function(pts_target, m) {
  V_X <- matrix(pts_target[, 1], nrow = length(pts_target[, 1]), ncol = 1)
  V_Y <- matrix(pts_target[, 2], nrow = length(pts_target[, 2]), ncol = 1)
  V_t <- matrix(pts_target[, 3], nrow = length(pts_target[, 3]), ncol = 1)
  
  MVX1 <- repMatColVRAI(V_X, length(V_X))
  MVX2 <- repMatLigneVRAI(V_X, length(V_X))
  
  MVY1 <- repMatColVRAI(V_Y, length(V_Y))
  MVY2 <- repMatLigneVRAI(V_Y, length(V_Y))
  
  MVY1 <- matrix(MVY1, length(V_Y), length(V_Y))
  MVY2 <- matrix(MVY2, length(V_Y), length(V_Y))
  
  MVX1 <- matrix(MVX1, length(V_X), length(V_X))
  MVX2 <- matrix(MVX2, length(V_X), length(V_X))
  
  M_ds = sqrt((MVX2 - MVX1) ^ 2 + (MVY2 - MVY1) ^ 2)
  MVt1 <- repMatColVRAI(V_t, length(V_t))
  MVt2 <- repMatLigneVRAI(V_t, length(V_t))
  
  MVt1 <- matrix(MVt1, length(V_t), length(V_t))
  MVt2 <- matrix(MVt2, length(V_t), length(V_t))
  
  
  M_dt <- abs(MVt2 - MVt1)
  to <- 1
  d = m[[1]] ^ (-2 * m[[2]])
  a = m[[3]] ^ (-2 * m[[4]])
  
  Elem <- a * M_dt ^ (2 * m[[4]]) + 1
  Sigma <-
    1. / (Elem ^ to) * exp(-d * (M_ds ^ (2. * m[[2]])) / (Elem ^ (m[[5]] *
                                                                    m[[2]])))
  
  Sigma_target <- (1 - m[[6]] ^ 2) * Sigma
  return(Sigma_target)
}


M_cov_obs <- function(pts_obs, nb_harddata, m) {
  V_X <- matrix(pts_obs[, 1], nrow = length(pts_obs[, 1]), ncol = 1)
  V_Y <- matrix(pts_obs[, 2], nrow = length(pts_obs[, 2]), ncol = 1)
  V_t <- matrix(pts_obs[, 3], nrow = length(pts_obs[, 3]), ncol = 1)
  
  
  MVX1 <- repMatColVRAI(V_X, length(V_X))
  MVX2 <- repMatLigneVRAI(V_X, length(V_X))
  
  MVY1 <- repMatColVRAI(V_Y, length(V_Y))
  MVY2 <- repMatLigneVRAI(V_Y, length(V_Y))
  
  MVX1 <- matrix(MVX1, length(V_X), length(V_X))
  MVX2 <- matrix(MVX2, length(V_X), length(V_X))
  
  MVY1 <- matrix(MVY1, length(V_Y), length(V_Y))
  MVY2 <- matrix(MVY2, length(V_Y), length(V_Y))
  
  M_ds = sqrt((MVX2 - MVX1) ^ 2 + (MVY2 - MVY1) ^ 2)
  
  rm(V_X)
  rm(V_Y)
  rm(MVX2)
  rm(MVX1)
  rm(MVY1)
  rm(MVY2)
  
  MVt1 <- repMatColVRAI(V_t, length(V_t))
  MVt2 <- repMatLigneVRAI(V_t, length(V_t))
  
  MVt1 <- matrix(MVt1, length(V_t), length(V_t))
  MVt2 <- matrix(MVt2, length(V_t), length(V_t))
  
  
  
  M_dt <- abs(MVt2 - MVt1)
  
  rm(MVt2)
  rm(MVt1)
  
  to <- 1
  c = m[[1]] ^ (-2 * m[[2]])
  a = m[[3]] ^ (-2 * m[[4]])
  
  Elem <- a * M_dt ^ (2 * m[[4]]) + 1
  Sigma <-
    1. / (Elem ^ to) * exp(-c * (M_ds ^ (2. * m[[2]])) / (Elem ^ (m[[5]] *
                                                                    m[[2]])))
  rm(M_dt)
  rm(Elem)
  
  Sigma_obs <- (1 - m[[6]] ^ 2) * Sigma
  si <- nrow(M_ds)
  
  M_noise <- matrix(0, si, si)
  if (nb_harddata > si) {
    M_noise <- diag(m[[6]] ^ 2, si, si)
  } else{
    for (h in (si - nb_harddata):si) {
      M_noise[h, h] <- m[[6]] ^ 2
    }
  }
  
  
  Sigma_obs <- Sigma_obs + M_noise
  rm(M_noise)
  
  return(Sigma_obs)
}


M_cross_cov <- function(pts_target, pts_obs, nb_harddata, m) {
  V_X_t = matrix(pts_target[, 1], nrow = length(pts_target[, 1]), ncol = 1)
  V_Y_t = matrix(pts_target[, 2], nrow = length(pts_target[, 2]), ncol = 1)
  V_X_o = matrix(pts_obs[, 1], nrow = length(pts_obs[, 1]), ncol = 1)
  V_Y_o = matrix(pts_obs[, 2], nrow = length(pts_obs[, 2]), ncol = 1)
  
  MVX1 <- repMatColVRAI2(V_X_t, length(V_X_t), length(V_X_o))
  MVX2 <- repMatLigneVRAI2(V_X_o, length(V_X_t), length(V_X_o))
  
  MVY1 <- repMatColVRAI2(V_Y_t, length(V_Y_t), length(V_Y_o))
  MVY2 <- repMatLigneVRAI2(V_Y_o, length(V_Y_t), length(V_Y_o))
  
  MVX1 <- matrix(MVX1, length(V_X_t), length(V_X_o))
  MVX2 <- matrix(MVX2, length(V_X_t), length(V_X_o))
  
  MVY1 <- matrix(MVY1, length(V_Y_t), length(V_Y_o))
  MVY2 <- matrix(MVY2, length(V_Y_t), length(V_Y_o))
  
  
  
  M_ds = sqrt((MVX2 - MVX1) ^ 2 + (MVY2 - MVY1) ^ 2)
  M_ds <- matrix(M_ds, length(V_Y_t), length(V_Y_o))
  
  V_time_t = matrix(pts_target[, 3], nrow = length(pts_target[, 3]), ncol = 1)
  V_time_o = matrix(pts_obs[, 3], nrow = length(pts_obs[, 1]), ncol = 1)
  MVt1 <- repMatColVRAI2(V_time_t, length(V_time_t), length(V_time_o))
  MVt2 <-
    repMatLigneVRAI2(V_time_o, length(V_time_t), length(V_time_o))
  
  MVt1 <- matrix(MVt1, length(V_time_t), length(V_time_o))
  MVt2 <- matrix(MVt2, length(V_time_t), length(V_time_o))
  rm(V_Y_o)
  rm(V_Y_t)
  rm(V_X_t)
  rm(MVY1)
  rm(MVX1)
  rm(MVX2)
  rm(MVY2)
  
  M_dt <- abs(MVt2 - MVt1)
  rm(MVt2)
  rm(MVt1)
  to <- 1
  a = m[[3]] ^ (-2 * m[[4]])
  
  Elem <- a * M_dt ^ (2 * m[[4]]) + 1
  c = m[[1]] ^ (-2 * m[[2]])
  Sigma_cross <-
    1. / (Elem ^ to) * exp(-c * (M_ds ^ (2. * m[[2]])) / (Elem ^ (m[[5]] *
                                                                    m[[2]])))
  
  Sigma_cross <- (1 - m[[6]] ^ 2) * Sigma_cross
  
  si <- nrow(M_ds)
  
  A <- matrix(0, nrow = si, ncol = length(V_X_o))
  for (g in 0:nb_harddata) {
    for (k in 0:length(V_X_o)) {
      A[g, k] <- m[[6]] ^ 2
    }
  }
  
  M_noise = (M_ds == 0) * (M_dt == 0) * A
  rm(A)
  Sigma_cross = Sigma_cross + M_noise
  rm(M_noise)
  return(Sigma_cross)
  
}


findInListInf <- function(L, a) {
  res <- list()
  for (i in 1:length(L)) {
    if (L[[i]] <= a) {
      res <- c(res, i)
    }
  }
  return(res)
  
}


findInList2 <-
  function(CondiRainTS_gaussian,
           Path_to_sim,
           i,
           j,
           step_t) {
    res <- list()
    for (g in 1:length(CondiRainTS_gaussian[[j]]$t)) {
      if ((CondiRainTS_gaussian[[j]]$t[g] > (Path_to_sim[[i]] - 10) * step_t) &&
          (CondiRainTS_gaussian[[j]]$t[g] < (Path_to_sim[[i]] + 10) * step_t)) {
        res <- c(res, g)
      }
    }
    return(res)
    
  }


repMatColVRAI <- function(M, n) {
  A <- matrix(0, n, n)
  for (i in 1:n) {
    for (j in 1:n) {
      A[i, j] <- M[[i]]
    }
  }
  return(A)
}


repMatLigneVRAI <- function(M, n) {
  A <- matrix(0, n, n)
  for (i in 1:n) {
    for (j in 1:n) {
      A[i, j] <- M[[j]]
    }
  }
  return(A)
}




repMatColVRAI2 <- function(M, n, m) {
  A <- matrix(0, n, m)
  for (i in 1:n) {
    for (j in 1:m) {
      A[i, j] <- M[[i]]
    }
  }
  return(A)
}


repMatLigneVRAI2 <- function(M, n, m) {
  A <- matrix(0, n, m)
  for (i in 1:n) {
    for (j in 1:m) {
      A[i, j] <- M[[j]]
    }
  }
  return(A)
}



minSpe <- function(L, V_ind_cond) {
  min <- L[[V_ind_cond[[1]]]]
  for (i in 1:length(V_ind_cond)) {
    if (L[[V_ind_cond[[i]]]] < min) {
      min <- L[[V_ind_cond[[i]]]]
    }
  }
  return(min)
  
}


maxSpe <- function(L, V_ind_cond) {
  max <- L[[V_ind_cond[[1]]]]
  for (i in 1:length(V_ind_cond)) {
    if (L[[V_ind_cond[[i]]]] > max) {
      max <- L[[V_ind_cond[[i]]]]
    }
  }
  return(max)
  
}
